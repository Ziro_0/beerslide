enum Listeners {
  ADD_POINT = 'add-point',
  LIVES_LOST = 'lives-lost',
  READY = 'ready',
}

export default Listeners;
