import { randomRange } from '../../util/util';
import JsonKey from '../data/JsonKey';

export default class ImpactAnim extends Phaser.Sprite {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly HIT_ANIM_KEY = 'hit';

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, frames: string[], scale: number) {
    super(game, x, y, JsonKey.GRAPHICS_IMAGES, frames[0]);
    
    this.scale.setTo(scale);

    this.animations.add(ImpactAnim.HIT_ANIM_KEY, frames, 20);
    
    this.events.onAnimationComplete.addOnce(() => {
      this.destroy();
    });

    this.play(ImpactAnim.HIT_ANIM_KEY);
  }
}
