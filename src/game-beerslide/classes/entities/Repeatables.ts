import { IImageKeyFramePair } from '../../IBeerSlideGameConfig';
import JsonKey from '../data/JsonKey';

type Repeatable = Phaser.Sprite;

// ==============================================================================================
export default class Repeatables {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  game: Phaser.Game;

  speed: number;

  private _freeRepeatables: Repeatable[];

  private _imageFrameKeys: IImageKeyFramePair[];

  private _group: Phaser.Group;
  
  private _usesPhysics: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, group: Phaser.Group, imageFrameKeys: IImageKeyFramePair[],
    usesPhysics: boolean, groupClassType?: any) {
    this.game = game;

    this._group = group;
    if (groupClassType) {
      this._group.classType = groupClassType;
    }

    this._imageFrameKeys = imageFrameKeys;
    this._usesPhysics = usesPhysics;
    this._freeRepeatables = [];
    this.speed = 1;
    this.init();
  }

  // ----------------------------------------------------------------------------------------------
  getAt(index = 0): Repeatable {
    return (<Repeatable>this._group.getAt(index));
  }

  // ----------------------------------------------------------------------------------------------
  render(): void {
    if (!this._group || !this._usesPhysics) {
      return;
    }

    this._group.forEach((repeatable: Repeatable) => {
      this.game.debug.body(repeatable);
    });
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (!this._group) {
      return;
    }

    this._group.forEach((repeatable: Repeatable) => {
      repeatable.x -= this.speed;
    });

    this.removeExpiredRepeatables();
    this.addNewRepeatables();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private addNewRepeatables(): void {
    const index = this._group.length - 1;
    let repeatable = <Repeatable>this._group.getAt(index);
    if (!repeatable) {
      return;
    }

    let x = Math.floor(repeatable.right) - 1;
    while (x < this.game.width + repeatable.width / 2) {
      repeatable = this.createRepeatable(x);
      x = (Math.floor(repeatable.right) - 1);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private createRepeatable(x: number): Repeatable {
    const repeatable = this.getNewRepeatable(x);
    this._group.add(repeatable);
    return (repeatable);
  }

  // ----------------------------------------------------------------------------------------------
  private getNewRepeatable(x: number): Repeatable {
    const imageFrame = Phaser.ArrayUtils.getRandomItem(this._imageFrameKeys);
    let repeatable = this._freeRepeatables.pop();
    if (repeatable) {
      repeatable.x = x;
      repeatable.loadTexture(imageFrame.key);
      repeatable.frameName = imageFrame.frame;
      this._group.add(repeatable);
    } else {
      repeatable = this._group.create(x, 0, imageFrame.key, imageFrame.frame);
    }

    return (repeatable);
  }

  // ----------------------------------------------------------------------------------------------
  private init(): void {
    let x = NaN;
    do {
      const repeatable = this.createRepeatable(x);
      if (Number.isNaN(x)) {
        repeatable.x = -Math.floor(repeatable.width / 2);
        x = repeatable.x;
      }

      x += (repeatable.width - 1);
    } while (x < this.game.width);
  }

  // ----------------------------------------------------------------------------------------------
  private removeExpiredRepeatables(): void {
    let repeatable = <Repeatable>this._group.getAt(0);
    while (repeatable && repeatable.right < -repeatable.width) {
      this.removeRepeatable(repeatable);
      repeatable = <Repeatable>this._group.getAt(0);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private removeRepeatable(repeatable: Repeatable): void {
    this._group.remove(repeatable);
    this._freeRepeatables.push(repeatable);
  }
}
