import JsonKey from '../data/JsonKey';
import { IObstacleObjectData } from '../data/ObstaclesData';

export default class Obstacle extends Phaser.Sprite {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _obstacleData: IObstacleObjectData

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, obstacleData: IObstacleObjectData) {
    super(game, x, y, JsonKey.GRAPHICS_IMAGES, obstacleData.frame);

    this._obstacleData = obstacleData;
    this.initPhysics();
  }

  // ----------------------------------------------------------------------------------------------
  getBody(): Phaser.Physics.Arcade.Body {
    return (this.body);
  }

  // ----------------------------------------------------------------------------------------------
  get obstacleData(): IObstacleObjectData {
    return (this._obstacleData);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private initPhysics(): void {
    this.game.physics.arcade.enable(this);

    const body = this.getBody();

    const boundingBox = this.obstacleData.boundingBox;
    const width = boundingBox.width || this.width;
    const height = boundingBox.height || this.height;
    body.setSize(width, height, boundingBox.x, boundingBox.y);

    body.immovable = true;
  }
}
