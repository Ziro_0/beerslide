export default class Platform extends Phaser.Sprite {
  // ==============================================================================================
  // properties
  // ==============================================================================================

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, key: string, imageFrame: string) {
    super(game, x, y, key, imageFrame);
    this.y = this.game.world.height - this.height;

    this.game.physics.arcade.enable(this);
    const body: Phaser.Physics.Arcade.Body = this.body;
    body.immovable = true;
  }
}
