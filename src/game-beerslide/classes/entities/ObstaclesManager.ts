import { Game } from '../../game';
import GameState from '../../states/GameState';
import { randomRange, removeTimer } from '../../util/util';
import JsonKey from '../data/JsonKey';
import ObstacleBoardsData, { IObstacleBoardData, IObstacleBoardObjectData } from '../data/ObstacleBoardsData';
import ObstaclesData, { IObstacleObjectData } from '../data/ObstaclesData';
import Obstacle from './Obstacle';

export default class ObstaclesManager {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  gameState: GameState;

  game: Game;

  timer: Phaser.Timer;

  speed: number;

  private _obstaclesData: ObstaclesData;
  private _obstacleBoardsData: ObstacleBoardsData;

  private _group: Phaser.Group;

  private spawnTimerEvent: Phaser.TimerEvent;

  private _minSpawnRateMs: number;
  private _maxSpawnRateMs: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, timer: Phaser.Timer, obstaclesGroup: Phaser.Group) {
    this.gameState = gameState;
    this.game = <Game>this.gameState.game;
    this.timer = timer;
    this._group = obstaclesGroup;
    this.speed = 0;
    this.initObstaclesData();
  }

  // ----------------------------------------------------------------------------------------------
  render(): void {
    this._group.forEach((obstacle: Obstacle) => {
      this.game.debug.body(obstacle);
    });
  }

  // ----------------------------------------------------------------------------------------------
  setSpawnRates(min: number, max?: number): void {
    this._minSpawnRateMs = min;
    this._maxSpawnRateMs = max || this._minSpawnRateMs;
  }

  // ----------------------------------------------------------------------------------------------
  start(): void {
    this.setSpwanTimer();
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    this.spawnTimerEvent = removeTimer(this.spawnTimerEvent);
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    this._updateObstacles();
    this.removeExpiredObstacles();
    this.setSpwanTimer();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private _createObstacle(obstacleBoardData: IObstacleBoardObjectData): void {
    const obstacleData: IObstacleObjectData =
      this._obstaclesData.obstacles.get(obstacleBoardData.id);
    if (!obstacleData) {
      return;
    }

    const x = this.game.width + (Number.isNaN(obstacleBoardData.x) ? 0 : obstacleBoardData.x);
    const obstacle = new Obstacle(this.game, x, 0, obstacleData);
    obstacle.y = Number.isNaN(obstacleBoardData.y)
      ? this.gameState.platformY - obstacle.height
      : obstacleBoardData.y;
    this._group.add(obstacle);
  }

  // ----------------------------------------------------------------------------------------------
  private createObstacleBoard(): void {
    const board = this.getRandomBoardData();
    if (!board) {
      return;
    }

    console.log(`ObstaclesManager. createObstacleBoard. id: ${board.id}`);

    board.objects.forEach((unit) => {
      this._createObstacle(unit);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private getRandomBoardData(): IObstacleBoardData {
    const boards = this._obstacleBoardsData ? this._obstacleBoardsData.boards.concat() : null;
    if (!boards) {
      return (null);
    }

    const gameSpeed = this.gameState.speed;

    while (boards.length > 0) {
      const index = Math.floor(Math.random() * boards.length);
      const board = boards[index];
      if (board.minSpeed <= gameSpeed && gameSpeed <= board.maxSpeed) {
        return (board);
      }
      
      Phaser.ArrayUtils.remove(boards, index);
    }

    return (null);
  }

  // ----------------------------------------------------------------------------------------------
  private getJsonData(key: string): any {
    const jsonData = this.game.cache.getJSON(key);
    if (!jsonData) {
      console.warn(`No JSON data with key ${key} found.`)
    }

    return (jsonData);
  }

  // ----------------------------------------------------------------------------------------------
  private initObstaclesData(): void {
    const jObstacleData = this.getJsonData(JsonKey.OBSTACLES_JSON_KEY);
    this._obstaclesData = new ObstaclesData(jObstacleData);

    const jObstacleBoardsData = this.getJsonData(JsonKey.OBSTACLE_BOARDS_JSON_KEY);
    this._obstacleBoardsData = new ObstacleBoardsData(jObstacleBoardsData);
  }

  // ----------------------------------------------------------------------------------------------
  private onTimerSpawn(): void {
    this.spawnTimerEvent = null;
    this.createObstacleBoard();
  }

  // ----------------------------------------------------------------------------------------------
  private removeExpiredObstacles(): void {
    let obstacle = <Obstacle>this._group.getAt(0);
    while (obstacle && obstacle.right < -obstacle.width) {
      obstacle.destroy();
      obstacle = <Obstacle>this._group.getAt(0);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private setSpwanTimer(): void {
    if (this._group.length > 0) {
      return;
    }

    if (this.spawnTimerEvent) {
      return;
    }

    const delay = randomRange(this._minSpawnRateMs, this._maxSpawnRateMs);
    this.spawnTimerEvent = this.timer.add(delay, this.onTimerSpawn, this);
  }

  // ----------------------------------------------------------------------------------------------
  private _updateObstacles(): void {
    this._group.forEach((obstacle: Obstacle) => {
      obstacle.x -= this.speed;
    });
  }
}
