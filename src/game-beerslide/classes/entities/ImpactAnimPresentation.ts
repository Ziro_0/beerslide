import GameState from '../../states/GameState';
import { getProperty, randomRange } from '../../util/util';
import ImpactAnim from './ImpactAnim';

export default class ImpactAnimPresentation {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  gameState: GameState;

  private _impactFrameKeys: string[][];

  private _impactScale: number[];

  private _bounds: Phaser.Rectangle;

  private _numHits: number;

  private _rateMs: number;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(gameState: GameState) {
    this.gameState = gameState;

    this.initBounds();
    this.initTimerConfig();
    this.initFrameKeys();
    this.initScale();
  }

  // ----------------------------------------------------------------------------------------------
  present(x: number, y: number): void {
    this._bounds.centerX = x;
    this._bounds.centerY = y;

    this.createHit();

    if (this._numHits >= 2) {
      this.gameState.timer.repeat(this._rateMs, Math.max(0, this._numHits - 1), this._onTimer,
        this);
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createHit(): void {
    const x = randomRange(this._bounds.left, this._bounds.right);
    const y = randomRange(this._bounds.top, this._bounds.bottom);
    const frameKeys = Phaser.ArrayUtils.getRandomItem(this._impactFrameKeys);
    const scale = randomRange(this._impactScale[0], this._impactScale[1]);
    const impact = new ImpactAnim(this.gameState.game, x, y, frameKeys, scale);
    this.gameState.obstaclesGroup.add(impact);
  }

  // ----------------------------------------------------------------------------------------------
  private initBounds(): void {
    const boundsData = this.gameState.gameObject.beerslideConfig.impactAnimationBounds;
    this._bounds = new Phaser.Rectangle(
      0, 0,
      getProperty(boundsData, 'x', 100),
      getProperty(boundsData, 'y', 100),
    );
  }

  // ----------------------------------------------------------------------------------------------
  private initFrameKeys(): void {
    const config = this.gameState.gameObject.beerslideConfig;
    this._impactFrameKeys = getProperty(config, 'impactFrameKeys', []);
  }

  // ----------------------------------------------------------------------------------------------
  private initScale(): void {
    const config = this.gameState.gameObject.beerslideConfig;
    this._impactScale = getProperty(config, 'impactScale', [ 1.0, 1.0]);
  }

  // ----------------------------------------------------------------------------------------------
  private initTimerConfig(): void {
    const config = this.gameState.gameObject.beerslideConfig;
    this._numHits = getProperty(config, 'impactAnimationNumHits', 5);
    this._rateMs = getProperty(config, 'impactAnimationRateMs', 100);
  }

  // ----------------------------------------------------------------------------------------------
  private _onTimer(): void {
    this.createHit();
  }
}