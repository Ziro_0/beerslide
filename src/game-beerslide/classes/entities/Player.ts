import { Game } from '../../game';
import GameState from '../../states/GameState';
import { getProperty, IRect, removeTimer } from '../../util/util';
import JsonKey from '../data/JsonKey';
import PlayerSoundsData from '../data/PlayerSoundsData';

export default class Player {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly BEGIN_DESCEND_YVEL_THRESHOLD = 50.0;
  
  private static readonly ANIM_KEY_FALL_BACK = 'fallBack';
  private static readonly ANIM_KEY_JUMP_ASCEND = 'jumpAscend';
  private static readonly ANIM_KEY_JUMP_DESCEND = 'jumpDescend';
  private static readonly ANIM_KEY_RUN = 'run';
  private static readonly ANIM_KEY_TRIP_LANDED = 'tripLanded';
  private static readonly ANIM_KEY_TRIPPED = 'tripped';

  private static readonly DEFAULT_GRAVITY = 1500;
  private static readonly DEFAULT_KNOCKBACK_X = 100;
  private static readonly DEFAULT_KNOCKBACK_Y = 200;
  private static readonly DEFAULT_JUMP_POWER = 1000.0;

  gameState: GameState;

  private _stepFrames: number[];

  private _soundsData: PlayerSoundsData;
  
  private _game: Game;

  private _sprite: Phaser.Sprite;

  private _graceTimerEvent: Phaser.TimerEvent;

  private _graceFlashTimerEvent: Phaser.TimerEvent;

  private _animRateSlowFps: number;
  private _animRateFastFps: number;
  private _speed: number;
  private _jumpPower: number;
  private _xDefeatedKnockback: number;
  private _yDefeatedKnockback: number;
  private _currentStepFrameIndex: number;

  private _hasDescentBegun: boolean;
  private _isDefeated: boolean;
  private _tripped: boolean;
  private _isInvincible: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(gameState: GameState, x: number, y: number, speed: number) {
    this.gameState = gameState;
    this._game = <Game>this.gameState.game;

    const config = this._game.beerslideConfig;
    const frameName = config.playerDefaultFrame;
    this._sprite = this._game.add.sprite(x, y, JsonKey.GRAPHICS_IMAGES, frameName);
    
    this._animRateSlowFps = config.playerAnimRateSlowFps;
    this._animRateFastFps = config.playerAnimRateFastFps;
    
    this.initPhysics();

    this._soundsData = new PlayerSoundsData(config.playerSounds);

    this._stepFrames = getProperty(config, 'playerStepFrames', []);

    this.setSpeed(speed);

    this.jumpPower = getProperty(config, 'playerJumpPower', Player.DEFAULT_JUMP_POWER);

    this._initKnockback();

    this.initAnimations();

    this._isDefeated = false;
  }

  // ----------------------------------------------------------------------------------------------
  get body(): Phaser.Physics.Arcade.Body {
    return (this.sprite.body);
  }

  // ----------------------------------------------------------------------------------------------
  defeatFallBack(): void {
    if (this.isDefeated) {
      return;
    }

    this._isDefeated = true;

    this.body.velocity.x = -this._xDefeatedKnockback;
    this.body.velocity.y = -this._yDefeatedKnockback;

    const fps = this.calcAnimRateFps();
    this._sprite.play(Player.ANIM_KEY_FALL_BACK, fps);

    this.applyDefeatedPositionOffset();
    this.setDefeatedBoundingBox();
  }
  
  // ----------------------------------------------------------------------------------------------
  defeatTripForward(): void {
    if (this.isDefeated) {
      return;
    }

    this._isDefeated = true;
    this._tripped = true;

    this.applyTrippedPositionOffset();
    this.setTrippedBoundingBox();

    this.body.velocity.x = this._xDefeatedKnockback;
    this.body.velocity.y = -this._yDefeatedKnockback;

    const fps = this.calcAnimRateFps();
    this._sprite.play(Player.ANIM_KEY_TRIPPED, fps);
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.setGrace(false);
    this._sprite.destroy();
    this.gameState = null;
    this._game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get hasDescentBegun(): boolean {
    return (this._hasDescentBegun);
  }

  // ----------------------------------------------------------------------------------------------
  get height(): number {
    return (this.sprite.height);
  }

  // ----------------------------------------------------------------------------------------------
  get isAirborne(): boolean {
    return (this.gameState.isPlayerAirborne());
  }

  // ----------------------------------------------------------------------------------------------
  get isDefeated(): boolean {
    return (this._isDefeated);
  }

  // ----------------------------------------------------------------------------------------------
  get isInvincible(): boolean {
    return (this._isInvincible);
  }

  // ----------------------------------------------------------------------------------------------
  jump(): void {
    if (this.isDefeated) {
      return;
    }

    const body = this.body;
    
    if (this.isAirborne) {
      return;
    }

    body.velocity.y = -this.jumpPower;
    this._hasDescentBegun = false;

    const fps = this.calcAnimRateFps();
    this.sprite.play(Player.ANIM_KEY_JUMP_ASCEND, fps);

    this.gameState.audio.play(this._soundsData.jumps);
  }

  // ----------------------------------------------------------------------------------------------
  get jumpPower(): number {
    return (this._jumpPower);
  }

  // ----------------------------------------------------------------------------------------------
  set jumpPower(value: number) {
    this._jumpPower = Math.abs(value);
  }

  // ----------------------------------------------------------------------------------------------
  playCrashSound(): void {
    this.gameState.audio.play(this._soundsData.crashes);
  }

  // ----------------------------------------------------------------------------------------------
  render(): void {
    this._game.debug.body(this.sprite);
  }

  // ----------------------------------------------------------------------------------------------
  run(): void {
    if (this.isDefeated) {
      return;
    }

    this._hasDescentBegun = undefined;
    const fps = this.calcAnimRateFps();
    this.sprite.play(Player.ANIM_KEY_RUN, fps);
  }

  // ----------------------------------------------------------------------------------------------
  setBoundingBox(rect: IRect): void {
    this.body.setSize(
      getProperty(rect, 'width', this.width),
      getProperty(rect, 'height', this.height),
      getProperty(rect, 'x', 0),
      getProperty(rect, 'y', 0),
    );
  }

  // ----------------------------------------------------------------------------------------------
  setGrace(isEnabled: boolean): void {
    this._isInvincible = isEnabled;

    this.destroyGraceTimers();
    this.sprite.alpha = 1.0;

    if (!isEnabled) {
      return;
    }

    this._graceFlashTimerEvent = this.gameState.timer.loop(50, () => {
      this.sprite.alpha = this.sprite.alpha === 0.25 ? 1.0 : 0.25;
    });

    this._graceTimerEvent = this.gameState.timer.add(1500, () => {
      this._graceFlashTimerEvent = removeTimer(this._graceFlashTimerEvent);
      this.setGrace(false);
    });
  }

  // ----------------------------------------------------------------------------------------------
  set speed(value: number) {
    if (this.isDefeated) {
      return;
    }

    this.setSpeed(value);
    this.sprite.animations.currentAnim.speed = this.calcAnimRateFps();
  }

  // ----------------------------------------------------------------------------------------------
  get sprite(): Phaser.Sprite {
    return (this._sprite);
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    this.body.velocity.set(0, 0);
  }

  // ----------------------------------------------------------------------------------------------
  get tripped(): boolean {
    return (this._tripped);
  }

  // ----------------------------------------------------------------------------------------------
  tripLanded(): void {
    if (!this.isDefeated) {
      return;
    }

    const fps = this.calcAnimRateFps();
    this.sprite.play(Player.ANIM_KEY_TRIP_LANDED, fps);
    this.applyTripLandedPositionOffset();
    this.setTripLandedBoundingBox();
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (!this.gameState) {
      return;
    }

    if (this.isDefeated) {
      this._updateDefeated();
    } else {
      this._updateOk();
    }
  }

  // ----------------------------------------------------------------------------------------------
  get width(): number {
    return (this.sprite.width);
  }
  
  // ----------------------------------------------------------------------------------------------
  get x(): number {
    return (this.sprite.x);
  }

  // ----------------------------------------------------------------------------------------------
  set x(value: number) {
    this.sprite.x = value;
  }

  // ----------------------------------------------------------------------------------------------
  get y(): number {
    return (this.sprite.y);
  }

  // ----------------------------------------------------------------------------------------------
  set y(value: number) {
    this.sprite.y = value;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private applyDefeatedPositionOffset(): void {
    const config = this._game.beerslideConfig;
    const x = getProperty(config, 'playerDefeatedPositionOffset.x', 0);
    const y = getProperty(config, 'playerDefeatedPositionOffset.y', 0);
    this.x += x;
    this.y += y;
  }

  // ----------------------------------------------------------------------------------------------
  private applyTripLandedPositionOffset(): void {
    const config = this._game.beerslideConfig;
    const x = getProperty(config, 'playerTrippedPositionOffset.x', 0);
    const y = getProperty(config, 'playerTrippedPositionOffset.y', 0);
    this.x += x;
    this.y += y;
  }

  // ----------------------------------------------------------------------------------------------
  private applyTrippedPositionOffset(): void {
    const config = this._game.beerslideConfig;
    const x = getProperty(config, 'playerTrippedPositionOffset.x', 0);
    const y = getProperty(config, 'playerTrippedPositionOffset.y', 0);
    this.x += x;
    this.y += y;
  }

  // ----------------------------------------------------------------------------------------------
  private calcAnimRateFps(): number {
    return (Phaser.Math.linear(this._animRateSlowFps, this._animRateFastFps, this._speed));
  }

  // ----------------------------------------------------------------------------------------------
  private destroyGraceTimers(): void {
    this._graceTimerEvent = removeTimer(this._graceTimerEvent);
    this._graceFlashTimerEvent = removeTimer(this._graceFlashTimerEvent);
  }

  // ----------------------------------------------------------------------------------------------
  private initAnimation(animKey: string, animFrameNames: string[], shouldLoop: boolean): void {
    const animFps = this.calcAnimRateFps();
    this.sprite.animations.add(animKey, animFrameNames, animFps, shouldLoop);
  }

  // ----------------------------------------------------------------------------------------------
  private initAnimations(): void {
    this.initAnimationsRun();
    this.initAnimationsJumpAscend();
    this.initAnimationsJumpDescend();
    this.initAnimationsFallBack();
    this.initAnimationsTripped();
    this.initAnimationsTripLanded();
  }

  // ----------------------------------------------------------------------------------------------
  private initAnimationsFallBack(): void {
    const frameNames = this.resolveFrameNames(this._game.beerslideConfig.playerFallBackFrames);
    const SHOULD_LOOP = false;
    this.initAnimation(Player.ANIM_KEY_FALL_BACK, frameNames, SHOULD_LOOP);
  }

  // ----------------------------------------------------------------------------------------------
  private initAnimationsJumpAscend(): void {
    const frameNames = this.resolveFrameNames(this._game.beerslideConfig.playerJumpAscendFrames);
    const SHOULD_LOOP = false;
    this.initAnimation(Player.ANIM_KEY_JUMP_ASCEND, frameNames, SHOULD_LOOP);
  }

  // ----------------------------------------------------------------------------------------------
  private initAnimationsJumpDescend(): void {
    const frameNames  = this.resolveFrameNames(this._game.beerslideConfig.playerJumpDescendFrames);
    const SHOULD_LOOP = false;
    this.initAnimation(Player.ANIM_KEY_JUMP_DESCEND, frameNames, SHOULD_LOOP);
  }

  // ----------------------------------------------------------------------------------------------
  private initAnimationsRun(): void {
    const frameNames  = this.resolveFrameNames(this._game.beerslideConfig.playerRunFrames);
    const SHOULD_LOOP = true;
    this.initAnimation(Player.ANIM_KEY_RUN, frameNames, SHOULD_LOOP);
  }

  // ----------------------------------------------------------------------------------------------
  private initAnimationsTripLanded(): void {
    const frameNames  = this.resolveFrameNames(this._game.beerslideConfig.playerTrippedLandFrames);
    const SHOULD_LOOP = false;
    this.initAnimation(Player.ANIM_KEY_TRIP_LANDED, frameNames, SHOULD_LOOP);
  }

  // ----------------------------------------------------------------------------------------------
  private initAnimationsTripped(): void {
    const frameNames  = this.resolveFrameNames(this._game.beerslideConfig.playerTrippedFrames);
    const SHOULD_LOOP = false;
    this.initAnimation(Player.ANIM_KEY_TRIPPED, frameNames, SHOULD_LOOP);
  }

  // ----------------------------------------------------------------------------------------------
  private initPhysics(): void {
    this._game.physics.arcade.enable(this.sprite);
    const body = this.body;
    body.bounce.y = 0;
    body.gravity.y = getProperty(this._game.beerslideConfig, 'playerGravity', Player.DEFAULT_GRAVITY);
  }

  // ----------------------------------------------------------------------------------------------
  private _initKnockback(): void {
    const config = this._game.beerslideConfig;
    const x = getProperty(config, 'playerDefeatedKnockback.x', Player.DEFAULT_KNOCKBACK_X);
    const y = getProperty(config, 'playerDefeatedKnockback.y', Player.DEFAULT_KNOCKBACK_Y);
    this._xDefeatedKnockback = x;
    this._yDefeatedKnockback = y;
  }

  // ----------------------------------------------------------------------------------------------
  private playJumpDescendAnim(): void {
    this._hasDescentBegun = true;

    const fps = this.calcAnimRateFps();
    this._sprite.play(Player.ANIM_KEY_JUMP_DESCEND, fps);
  }

  // ----------------------------------------------------------------------------------------------
  private resolveFrameNames(frameNames: string[]): string[] {
    return (frameNames || [ this._game.beerslideConfig.playerDefaultFrame ]);
  }

  // ----------------------------------------------------------------------------------------------
  private setDefeatedBoundingBox(): void {
    const config = this._game.beerslideConfig;
    const box = config.playerDefeatedBoundingBox || config.playerBoundingBox;
    this.setBoundingBox(box);
  }

  // ----------------------------------------------------------------------------------------------
  private setTripLandedBoundingBox(): void {
    const config = this._game.beerslideConfig;
    let box = config.playerTrippedLandBoundingBox || config.playerTrippedBoundingBox;
    box = box || config.playerBoundingBox;
    this.setBoundingBox(box);
  }

  // ----------------------------------------------------------------------------------------------
  private setTrippedBoundingBox(): void {
    const config = this._game.beerslideConfig;
    const box = config.playerTrippedBoundingBox || config.playerBoundingBox;
    this.setBoundingBox(box);
  }

  // ----------------------------------------------------------------------------------------------
  private setSpeed(value: number): void {
    this._speed = Phaser.Math.clamp(value, 0.0, 1.0);
  }

  // ----------------------------------------------------------------------------------------------
  private shouldPlayJumpDescendAnim(): boolean {
    if (this._hasDescentBegun) {
      return (false);
    }

    const body = this.body;
    if (!this.isAirborne) {
      return (false);
    }

    return (body.velocity.y > Player.BEGIN_DESCEND_YVEL_THRESHOLD);
  }

  // ----------------------------------------------------------------------------------------------
  private _updateDefeated(): void {
    // TODO
  }

  // ----------------------------------------------------------------------------------------------
  private _updateOk(): void {
    if (this.isAirborne) {
      this._updateJumpAnimation();
    } else {
      this._updateStep();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _updateJumpAnimation(): void {
    if (this.shouldPlayJumpDescendAnim()) {
      this.playJumpDescendAnim();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _updateStep(): void {
    if (!this._stepFrames || !this._stepFrames.length) {
      return;
    }

    // volatile! use of private member, `Phaser.Animation._frameIndex`, but it's the only way to
    // get the zero-based local frame index of the current animation.
    const frameIndex = this.sprite.animations.currentAnim['_frameIndex'];
    if (frameIndex === this._currentStepFrameIndex) {
      return;
    }

    this._currentStepFrameIndex = frameIndex;

    if (this._stepFrames.indexOf(frameIndex) > -1) {
      this.gameState.audio.play(this._soundsData.steps);
    }
  }
}
