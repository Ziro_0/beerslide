import { Easing } from 'phaser-ce';
import JsonKey from '../data/JsonKey';

export default class SpeedMessage extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, y: number, key: string) {
    super(game, game.world.centerX, y, JsonKey.GRAPHICS_IMAGES, key);

    this.anchor.set(0.5, 0.5);

    const DURATION = 3000;
    const tween = this.game.tweens.create(this).to({
        y: y + 100,
        alpha: 0,
      },
      DURATION,
      Easing.Linear.None,
      true);

    tween.onComplete.addOnce(this.onTweenComplete, this);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private onTweenComplete(): void {
    this.destroy();
  }
}
