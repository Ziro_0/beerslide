import { getProperty, IRect } from '../../util/util';
import TripImpact from './TripImpact';

// ================================================================================================
export interface IObstacleObjectData {
  boundingBox?: IRect;
  id?: string;
  frame?: string;
  impact?: string;
  impactSounds?: string[];
  impactTripSounds?: string[];
}

// ================================================================================================
export default class ObstaclesData {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  obstacles: Map<string, IObstacleObjectData>;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(jObstaclesData: any) {
    this.setData(jObstaclesData);
  }

  // ----------------------------------------------------------------------------------------------
  setData(jObstaclesData: any): void {
    this.obstacles = new Map();

    if (!Array.isArray(jObstaclesData)) {
      console.warn('The data supplied must be an array.');
      return;
    }

    jObstaclesData.forEach((jUnit: any) => {
      const boardData: IObstacleObjectData = this.createDataUnit(jUnit);
      if (boardData.id) {
        this.obstacles.set(boardData.id, boardData);
      } else {
        console.warn('No id found for obstacle data:');
        console.warn(boardData);
      }
    });
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private createDataUnit(jUnit: any): IObstacleObjectData {
    const objectData: IObstacleObjectData = {
      id: getProperty(jUnit, 'id', ''),
      frame: getProperty(jUnit, 'frame', ''),
      boundingBox: {
        x: getProperty(jUnit, 'boundingBox.x', 0),
        y: getProperty(jUnit, 'boundingBox.y', 0),
        width: getProperty(jUnit, 'boundingBox.width', 0),
        height: getProperty(jUnit, 'boundingBox.height', 0),
      },

      impact: getProperty(jUnit, 'impact', TripImpact.DEFAULT),
      impactSounds: getProperty(jUnit, 'impactSounds', null),
      impactTripSounds: getProperty(jUnit, 'impactTripSounds', null),
    };

    return (objectData);
  }
}
