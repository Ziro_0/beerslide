enum TripImpact {
  DEFAULT = 'default',
  TRIP = 'trip',
  TRIP_IF_AIRBORNE = 'trip-if-airborne',
};

export default TripImpact;
