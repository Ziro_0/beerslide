import IBeerSlideGameConfig from '../../IBeerSlideGameConfig'
import JsonKey from './JsonKey';

export default function CreateConfigData(): IBeerSlideGameConfig {
  const data: IBeerSlideGameConfig = {
    backgroundMaxScrollSpeed: 3,
    backgroundMinScrollSpeed: 1.5,

    platformImageFrameKeys: [
      {
        key: JsonKey.GRAPHICS_IMAGES,
        frame: 'platform-00',
      }
    ],

    backgroundImageFrameKeys: [
      {
        key: JsonKey.GRAPHICS_BACKGROUNDS_0,
        frame: 'bg-1',
      },
      {
        key: JsonKey.GRAPHICS_BACKGROUNDS_0,
        frame: 'bg-2',
      },
      {
        key: JsonKey.GRAPHICS_BACKGROUNDS_1,
        frame: 'bg-3',
      },
    ],

    groundMaxScrollSpeed: 18,
    groundMinScrollSpeed: 10,

    impactAnimationBounds: {
      x: 1,
      y: 1,
    },

    impactAnimationNumHits: 1,

    impactFrameKeys: [
      [
        'shock-00-00',
        'shock-00-01',
      ],
      [
        'shock-01-00',
        'shock-01-01',
      ],
      [
        'shock-02-00',
        'shock-02-01',
      ]
    ],

    impactScale: [
      1.5,
      2.5,
    ],

    playerAnimRateSlowFps: 5,
    playerAnimRateFastFps: 12,
    
    playerBoundingBox: {
      x: 42,
      y: 35,
      width: 95,
      height: 105,
    },

    playerDefaultFrame: 'player-run00',

    playerDefeatedKnockback: {
      x: 0,
      y: 0,
    },

    playerFallBackFrames: [
      'player-fall-back00',
      'player-fall-back01',
    ],

    playerGravity: 3500,

    playerJumpAscendFrames: [
      'player-jump00',
    ],

    playerJumpDescendFrames: [
      'player-jump00',
    ],

    playerJumpPower: 1500,

    playerRunFrames: [
      'player-run00',
      'player-run01',
      'player-run02',
      'player-run03',
      'player-run04',
      'player-run05',
      'player-run06',
    ],

    playerSounds: {
      jumps: [
        'player_jump00',
        'player_jump01',
      ],
    
      crashes: [
        'player_collide00',
      ],
    },

    playerStartPosition: {
      x: 96,
    },

    playerTrippedFrames: [
      'player-tripping00',
    ],

    playerTrippedLandFrames: [
      'player-trip-land00',
    ],

    shouldRemoveObstacleOnCollision: true,

    sounds: [
      'player_collide00',
      'player_jump00',
      'player_jump01'
    ],

    startSpeed: 0.0,

    timeWithinSpeed: 8.0,

    useCameraEffectsOnCollision: false,

    useSpeedChangeMessages: true,
  };

  return (data);
}
