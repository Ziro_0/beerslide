enum JsonKey {
  GRAPHICS_BACKGROUNDS_0 = 'backgrounds-0',
  GRAPHICS_BACKGROUNDS_1 = 'backgrounds-1',
  GRAPHICS_IMAGES = 'images',
  OBSTACLE_BOARDS_JSON_KEY = 'obstacleBoards',
  OBSTACLES_JSON_KEY = 'obstacles',
};

export default JsonKey;
