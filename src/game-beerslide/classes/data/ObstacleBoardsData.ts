import { getProperty } from '../../util/util';

// ================================================================================================
export interface IObstacleBoardObjectData {
  id?: string;
  x?: number;
  y?: number;
}

// ================================================================================================
export interface IObstacleBoardData {
  id?: string;
  objects?: IObstacleBoardObjectData[];
  minSpeed?: number;
  maxSpeed?: number;
}

// ================================================================================================
export default class ObstacleBoardsData {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  boards: IObstacleBoardData[];

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(jObstacleBoardsData: any) {
    this.setData(jObstacleBoardsData);
  }

  // ----------------------------------------------------------------------------------------------
  setData(jObstacleBoardsData: any[]): void {
    this.boards = [];

    if (!Array.isArray(jObstacleBoardsData)) {
      console.warn('The data supplied must be an array.');
      return;
    }

    jObstacleBoardsData.forEach((jUnit: any) => {
      this.boards.push(this.createDataUnit(jUnit));
    });
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private createDataUnit(jUnit: any): IObstacleBoardData {
    const data: IObstacleBoardData = {
      id: getProperty(jUnit, 'id', `prop${Math.random()}`),
      objects: this.parseObjectsData(getProperty(jUnit, 'objects', [])),
      minSpeed: getProperty(jUnit, 'minSpeed', 0.0),
      maxSpeed: getProperty(jUnit, 'maxSpeed', 1.0),
    };

    return (data);
  }

  // ----------------------------------------------------------------------------------------------
  private parseObjectsData(jData: any): IObstacleBoardObjectData[] {
    const objectsData: IObstacleBoardObjectData[] = [];
    const jUnits = Array.isArray(jData) ? jData : [ jData ];

    jUnits.forEach((jUnit: any) => {
      const objectData: IObstacleBoardObjectData = {
        id: getProperty(jUnit, 'id', ''),
        x: getProperty(jUnit, 'x', NaN),
        y: getProperty(jUnit, 'y', NaN),
      };

      objectsData.push(objectData);
    });

    return (objectsData);
  }
}
