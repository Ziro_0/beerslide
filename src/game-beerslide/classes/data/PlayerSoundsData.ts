import { IPlayerSounds } from '../../IBeerSlideGameConfig';
import { getProperty } from '../../util/util';

// ================================================================================================
export default class PlayerSoundsData {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  jumps: string[];

  steps: string[];

  crashes: string[];

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(data: IPlayerSounds) {
    this.setData(data);
  }

  // ----------------------------------------------------------------------------------------------
  setData(data: IPlayerSounds): void {
    this.jumps = PlayerSoundsData.GetSoundKeys(data, 'jumps');
    this.steps = PlayerSoundsData.GetSoundKeys(data, 'steps');
    this.crashes = PlayerSoundsData.GetSoundKeys(data, 'crashes');
  }

  // ==============================================================================================
  // private
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  private static GetSoundKeys(jData: any, key: string): string[] {
    const rawData: any = getProperty(jData, key, []);
    return (Array.isArray(rawData) ? rawData : [rawData]);
  }
}
