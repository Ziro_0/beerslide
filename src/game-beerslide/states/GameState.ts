import AudioPlayer from '../classes/audio/AudioPlayer';
import JsonKey from '../classes/data/JsonKey';
import TripImpact from '../classes/data/TripImpact';
import ImpactAnimPresentation from '../classes/entities/ImpactAnimPresentation';
import Obstacle from '../classes/entities/Obstacle';
import ObstaclesManager from '../classes/entities/ObstaclesManager';
import Platform from '../classes/entities/Platform';
import Player from '../classes/entities/Player';
import Repeatables from '../classes/entities/Repeatables';
import SpeedMessage from '../classes/ui/SpeedMessage';
import { Game } from '../game';
import Listeners from '../Listeners';
import { getProperty, randomRange, removeTimer } from '../util/util';

export default class GameState extends Phaser.State {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly DEFAULT_POINTS_GAINED = 1;
  private static readonly DEFAULT_POINTS_LOST = 3;

  onPlayerResurrected: Phaser.Signal;

  private _audioPlayer: AudioPlayer;
  
  private _backgrounds: Repeatables;
  private _platforms: Repeatables;

  private _player: Player;
  private _obstaclesManager: ObstaclesManager;

  private _backgroundGroup: Phaser.Group;
  private _repeatableBackgroundsGroup: Phaser.Group;
  private _playerGroup: Phaser.Group;
  private _obstaclesGroup: Phaser.Group;
  private _platformsGroup: Phaser.Group;
  private _uiGroup: Phaser.Group;

  private _incorrectOrientationImage: Phaser.Image;

  private _timer: Phaser.Timer;
  private _scoreTimerEvent: Phaser.TimerEvent;
  private _speedTimerEvent: Phaser.TimerEvent;

  private _speed: number;
  private _isPlayerAirborneTicks: number;

  private _playerCollidedWithGroundAfterDefeat: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    super();
  }

  // ----------------------------------------------------------------------------------------------
  get audio(): AudioPlayer {
    return (this._audioPlayer);
  }

  // ----------------------------------------------------------------------------------------------
  create() {
    console.log('Game state [create] started');

    this.onPlayerResurrected = new Phaser.Signal();
    this.onPlayerResurrected.add(this.onPlayerResurrectedCb, this);

    this.game.stage.backgroundColor = '#000000';
    
    this.initOrientation();

    this._speed = 0;

    this.initTimer();
    this.initGroups();
    this.initAudioPlayer();
    this.initUi();
    this.initBackground();
    
    this.gameObject.physics.startSystem(Phaser.Physics.ARCADE);

    this.initRepeatableBackgrounds();
    this.initRepeatablePlatforms();
    this.initObstaclesManager();
    this.initPlayer();

    this.startGame();

    this.listenerCallback(Listeners.READY, this.game);
  }

  // ----------------------------------------------------------------------------------------------
  get gameObject(): Game {
    return (<Game>this.game);
  }

  // ----------------------------------------------------------------------------------------------
  isPlayerAirborne(): boolean {
    return (this._isPlayerAirborneTicks >= 3);
  }

  // ----------------------------------------------------------------------------------------------
  get obstaclesGroup(): Phaser.Group {
    return (this._obstaclesGroup);
  }

  // ----------------------------------------------------------------------------------------------
  get platformY(): number {
    return (this._platforms.getAt(0).y);
  }

  // ----------------------------------------------------------------------------------------------
  shutdown(): void {
    this.onPlayerResurrected.dispose();
    this._obstaclesManager.stop();
    this.removeScoreTimer();
    this.removeSpeedTimer();
    this.audio.dispose();
    this.game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get timer(): Phaser.Timer {
    return (this._timer);
  }

  // ----------------------------------------------------------------------------------------------
  get uiGroup(): Phaser.Group {
    return (this._uiGroup);
  }

  // ----------------------------------------------------------------------------------------------
  render(): void {
    if (this.gameObject.beerslideConfig.debugShowBoundingBoxes) {
      this._platforms.render();
      this._obstaclesManager.render();
      this._player.render();
    }
  }

  // ----------------------------------------------------------------------------------------------
  get speed(): number {
    return (this._speed);
  }

  // ----------------------------------------------------------------------------------------------
  set speed(value: number) {
    this._speed = Phaser.Math.clamp(value, 0.0, 1.0);
    this._player.speed = value;

    this._backgrounds.speed = this.calcBackgroundScrollSpeed();
    
    const foregroundSpeed = this.calcPlatformsScrollSpeed();
    this._platforms.speed = foregroundSpeed;
    this._obstaclesManager.speed = foregroundSpeed;
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (!this._player) {
      return;
    }

    if (this._player.isDefeated) {
      this._updatePlayerDefeated();
    } else {
      this._updatePlayerOk();
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private calcBackgroundScrollSpeed(): number {
    const config = this.gameObject.beerslideConfig;
    return (this.calcScrollSpeed(
      config.backgroundMinScrollSpeed,
      config.backgroundMaxScrollSpeed,
    ));
  }

  // ----------------------------------------------------------------------------------------------
  private calcScrollSpeed(min: number, max: number): number {
    return (Phaser.Math.linear(min, max, this._speed));
  }

  // ----------------------------------------------------------------------------------------------
  private calcPlatformsScrollSpeed(): number {
    const config = this.gameObject.beerslideConfig;
    return (this.calcScrollSpeed(
      config.groundMinScrollSpeed,
      config.groundMaxScrollSpeed,
    ));
  }

  // ----------------------------------------------------------------------------------------------
  private calcViewOffset(): number {
    const headerBarDiv = document.getElementById('headerBar');
    return (headerBarDiv ? headerBarDiv.clientHeight * this.game.scale.scaleFactor.y : 0);
  }

  // ----------------------------------------------------------------------------------------------
  private defeatPlayerFallback(obstacle: Obstacle): void {
    this._player.defeatFallBack();
    this._obstaclesManager.stop();

    this._backgrounds.speed = 0;
    const foregroundSpeed = 0;

    this._platforms.speed = foregroundSpeed;
    this._obstaclesManager.speed = foregroundSpeed;

    this.audio.play(obstacle.obstacleData.impactSounds);

    this.presentImpactAnimation(obstacle);
  }

  // ----------------------------------------------------------------------------------------------
  private defeatPlayerTripForward(obstacle: Obstacle): void {
    this._player.defeatTripForward();
    this._obstaclesManager.stop();
    
    const obstacleData = obstacle.obstacleData;
    this.audio.play(obstacleData.impactTripSounds || obstacleData.impactSounds);
    
    this.presentImpactAnimation(obstacle);
  }

  // ----------------------------------------------------------------------------------------------
  private destroyPlayer(): void {
    if (this._player) {
      this._player.destroy();
      this._player = null;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private gameOver(): void {
    this.listenerCallback(Listeners.LIVES_LOST, this.gameObject);
  }

  // ----------------------------------------------------------------------------------------------
  private getPlatform(index = 0): Platform {
    return (this._platforms.getAt(index));
  }

  // ----------------------------------------------------------------------------------------------
  private handleDefeatedPlayerCollidedWithPlatform(): void {
    if (this._playerCollidedWithGroundAfterDefeat) {
      return;
    }

    this._playerCollidedWithGroundAfterDefeat = true;
    this._player.stop();
    
    if (this._player.tripped) {
      this._player.tripLanded();
    }

    this._player.playCrashSound();

    const target = {
      value: 1.0,
    };

    const tween = this.add.tween(target).to(
      {
        value: 0,
      },
      1000,
      Phaser.Easing.Linear.None,
      true,
    );

    tween.onComplete.addOnce(() => {
      this._backgrounds.speed = 0;
      this._platforms.speed = 0;
      this._obstaclesManager.speed = 0;
      this.onPlayerDefeatedDelayTweenComplete();
    });

    const bgSpeed = this._backgrounds.speed;
    const platformsSpeed = this._platforms.speed;
    const obstaclesSpeed = this._obstaclesManager.speed;
    tween.onUpdateCallback(() => {
      const ratio = target.value;
      this._backgrounds.speed = bgSpeed * ratio;
      this._platforms.speed = platformsSpeed * ratio;
      this._obstaclesManager.speed = obstaclesSpeed * ratio;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private hideIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      this._incorrectOrientationImage.destroy();
      this._incorrectOrientationImage = null;
    }

    this.game.paused = false;
  }

  // ----------------------------------------------------------------------------------------------
  private initAudioPlayer(): void {
    this._audioPlayer = new AudioPlayer(this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private initBackground(): void {
    const imageFrameKey = this.gameObject.beerslideConfig.backgroundImageKey;
    if (imageFrameKey) {
      this.game.add.image(0, 0, JsonKey.GRAPHICS_IMAGES,
        imageFrameKey, this._backgroundGroup);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private initObstaclesManager(): void {
    this._obstaclesManager = new ObstaclesManager(this, this.timer, this._obstaclesGroup);
    this._obstaclesManager.setSpawnRates(500, 500);
  }

  // ----------------------------------------------------------------------------------------------
  private initOrientation(): void {
    this.scale.onOrientationChange.add((scale: Phaser.ScaleManager,
      prevOrientation: string, wasIncorrect: boolean) => {
        const hasChanged = scale.screenOrientation !== prevOrientation;
        if (!hasChanged) {
          return;
        }

        const isIncorrect = scale.incorrectOrientation && !wasIncorrect;
        if (isIncorrect) {
          this.showIncorrectOrientationMessage();
        } else {
          this.hideIncorrectOrientationMessage();
        }
      });
  }

  // ----------------------------------------------------------------------------------------------
  private initRepeatableBackgrounds(): void {
    const imageFrameKeys = this.gameObject.beerslideConfig.backgroundImageFrameKeys;
    this._backgrounds = new Repeatables(this.game, this._repeatableBackgroundsGroup,
      imageFrameKeys, false);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroup(enableBody = false): Phaser.Group {
    const yOffset = 0;
    const group = this.game.add.group();
    
    if (enableBody) {
      group.enableBody = true;
    }

    group.y = yOffset;
    return (group);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroups(): void {
    this._backgroundGroup = this.initGroup();
    this._repeatableBackgroundsGroup = this.initGroup();
    this._playerGroup = this.initGroup();
    this._obstaclesGroup = this.initGroup();
    this._platformsGroup = this.initGroup();
    this._uiGroup = this.initGroup();
  }

  // ----------------------------------------------------------------------------------------------
  private initPlayer(isGraceEnabled = false): void {
    this.destroyPlayer();

    const config = this.gameObject.beerslideConfig;
    
    this._player = new Player(this, 0, 0,
      getProperty(config, 'startSpeed', 0.25),
    );

    this._player.setBoundingBox(config.playerBoundingBox);

    const position = config.playerStartPosition;
    this._player.x = getProperty(position, 'x', 96);
    this._player.y = getProperty(position, 'y',
      this.getPlatform().y - this._player.height);

    this._playerGroup.add(this._player.sprite);
    this._isPlayerAirborneTicks = 0;

    this._player.setGrace(isGraceEnabled);

    this._playerCollidedWithGroundAfterDefeat = false;
  }

  // ----------------------------------------------------------------------------------------------
  private initRepeatablePlatforms(): void {
    const imageFrameKeys = this.gameObject.beerslideConfig.platformImageFrameKeys;
    this._platforms = new Repeatables(this.game, this._platformsGroup, imageFrameKeys, 
      true, Platform);
  }

  // ----------------------------------------------------------------------------------------------
  private initScoreTimer(): void {
    this._scoreTimerEvent = this.timer.loop(1000, () => {
      const pointsGained = getProperty(this.gameObject.beerslideConfig,
        'pointsGainedPerStep', GameState.DEFAULT_POINTS_GAINED);
      this.listenerCallback(Listeners.ADD_POINT, pointsGained);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private initSpeedTimer(): void {
    const config = this.gameObject.beerslideConfig;
    const delay = getProperty(config, 'timeWithinSpeed', 8.0);
    if (delay > 0) {
      this._speedTimerEvent = this.timer.add(delay * 1000, this.onSpeedTimer, this);
    }
  }
  
  // ----------------------------------------------------------------------------------------------
  private initTimer(): void {
    this._timer = this.game.time.create(false);
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private initUi(): void {
    this.input.onDown.add(this.onInputDownPlayerJump, this);
    
    [
      Phaser.KeyCode.ENTER,
      Phaser.KeyCode.NUMPAD_ENTER,
      Phaser.KeyCode.SPACEBAR,
    ].forEach((keyCode) => {
      const key = this.input.keyboard.addKey(keyCode);
      key.onDown.add(this.onInputDownPlayerJump, this);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private listenerCallback(key: string, ...args): void {
    const callback: Function = this.gameObject.listenerMapping[key];
    if (callback) {
      callback.call(this.game, ...args);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onInputDownPlayerJump(): void {
    this._player.jump();
  }

  // ----------------------------------------------------------------------------------------------
  private onPlayerCollidedWithPlatform(): void {
    this._isPlayerAirborneTicks = 0;

    if (this._player.hasDescentBegun) {
      this._player.run();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onPlayerDefeatedDelayTweenComplete(): void {
    const pointsLost = getProperty(this.gameObject.beerslideConfig, 'pointsLostWhenLifeLost',
      GameState.DEFAULT_POINTS_LOST);
    this.listenerCallback(Listeners.LIVES_LOST, this.gameObject, pointsLost);
  }

  // ----------------------------------------------------------------------------------------------
  private onPlayerOvelappedObstacle(playerSprite: Phaser.Sprite, obstacle: Obstacle): void {
    this.removeScoreTimer();
    this.removeSpeedTimer();

    const config = this.gameObject.beerslideConfig;
    if (getProperty(config, 'shouldRemoveObstacleOnCollision', true)) {
      obstacle.visible = false;
    }

    if (this.isPlayerAirborne()) {
      if (this.wouldPlayerBeTripped(obstacle)) {
        this.defeatPlayerTripForward(obstacle);
        return;
      }
    }

    this.defeatPlayerFallback(obstacle);

    if (getProperty(config, 'useCameraEffectsOnCollision', true)) {
      this.camera.shake(0.01, 400);
      this.camera.flash(0xffffff, 50);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onPlayerResurrectedCb(): void {
    this.initPlayer(true);
    this.startGame();
  }

  // ----------------------------------------------------------------------------------------------
  private onPlayerWouldOverlapObstacle(playerSprite: Phaser.Sprite,
    obstacle: Obstacle): boolean {
    return (!this._player.isDefeated);
  }

  // ----------------------------------------------------------------------------------------------
  private onSpeedTimer(): void {
    this._speedTimerEvent = null;

    const speed = this.speed > 0.5
      ? randomRange(0, 0.2)
      : randomRange(0.8, 1.0)

    const DURATION = 2500;
    const tween = this.tweens.create(this).to(
      {
        'speed': speed,
      },
      DURATION,
      Phaser.Easing.Linear.None,
      true,
    );

    tween.onComplete.addOnce(this.onSpeedTweenComplete, this);

    this.showSpeedMessage(speed);
  }

  // ----------------------------------------------------------------------------------------------
  private onSpeedTweenComplete(): void {
    this.initSpeedTimer();
  }

  // ----------------------------------------------------------------------------------------------
  private presentImpactAnimation(obstacle: Obstacle): void {
    const impactAnim = new ImpactAnimPresentation(this);
    impactAnim.present(this._player.sprite.centerX, this._player.sprite.centerY);
  }

  // ----------------------------------------------------------------------------------------------
  private removeScoreTimer(): void {
    this._scoreTimerEvent = removeTimer(this._scoreTimerEvent);
  }

  // ----------------------------------------------------------------------------------------------
  private removeSpeedTimer(): void {
    this._speedTimerEvent = removeTimer(this._speedTimerEvent);
  }

  // ----------------------------------------------------------------------------------------------
  private showIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      return;
    }

    this.game.paused = true;
    const y = this.calcViewOffset();
    this._incorrectOrientationImage = this.add.image(0, y, JsonKey.GRAPHICS_IMAGES,
      'incorrect-orientation-message');
  }

  // ----------------------------------------------------------------------------------------------
  private showSpeedMessage(newSpeed: number): void {
    const config = this.gameObject.beerslideConfig;
    if (!config.useSpeedChangeMessages) {
      return;
    }

    const isAccel = newSpeed > this.speed;
    const msgFrame = isAccel ? 'speed-msg-accel' : 'speed-msg-decel';
    const yOffset = this.calcViewOffset() + 100;
    const image = new SpeedMessage(this.game, yOffset, msgFrame);
    this.uiGroup.add(image);
  }

  // ----------------------------------------------------------------------------------------------
  private startGame(): void {
    const config = this.gameObject.beerslideConfig;
    this.speed = config.startSpeed;
    this._player.run();
    this._obstaclesManager.start();
    this.initScoreTimer();
    this.initSpeedTimer();
  }
  
  // ----------------------------------------------------------------------------------------------
  private _updatePlayerDefeated(): void {
    this._backgrounds.update();
    this._platforms.update();
    this._obstaclesManager.update();
    this._player.update();
    
    if (this.game.physics.arcade.collide(this._player.sprite, this._platformsGroup)) {
      this.handleDefeatedPlayerCollidedWithPlatform();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _updatePlayerOk(): void {
    this._backgrounds.update();
    this._platforms.update();
    this._obstaclesManager.update();
    this._player.update();

    if (!this.game.physics.arcade.collide(this._player.sprite,
    this._platformsGroup, this.onPlayerCollidedWithPlatform, null, this)) {
      this._isPlayerAirborneTicks += 1;
    }
    
    if (!this._player.isInvincible) {
      this.game.physics.arcade.overlap(this._player.sprite, this._obstaclesGroup,
        this.onPlayerOvelappedObstacle, this.onPlayerWouldOverlapObstacle, this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private wouldPlayerBeTripped(obstacle: Obstacle): boolean {
    const impact = obstacle.obstacleData.impact;
    
    switch (impact) {
      case TripImpact.TRIP:
        return (this._wouldPlayerBeTrippedTrip(obstacle));

      case TripImpact.TRIP_IF_AIRBORNE:
        return (this._wouldPlayerBeTrippedIfAirborne(obstacle));

      case TripImpact.DEFAULT:
      default:
        return (this._wouldPlayerBeTrippedDefault(obstacle));
    }
  }

  // ----------------------------------------------------------------------------------------------
  private _wouldPlayerBeTrippedDefault(obstacle: Obstacle): boolean {
    const rectPlayer = new Phaser.Rectangle();
    this._player.body.getBounds(rectPlayer);

    const rectObstacle = new Phaser.Rectangle();
    obstacle.getBody().getBounds(rectObstacle);

    const rectIntersect = new Phaser.Rectangle();
    rectPlayer.intersection(rectObstacle, rectIntersect);

    if (rectIntersect.empty) {
      return (false);
    }

    return (rectIntersect.height < rectIntersect.width);
  }

  // ----------------------------------------------------------------------------------------------
  private _wouldPlayerBeTrippedIfAirborne(obstacle: Obstacle): boolean {
    return (this.isPlayerAirborne());
  }

  // ----------------------------------------------------------------------------------------------
  private _wouldPlayerBeTrippedTrip(obstacle: Obstacle): boolean {
    return (true);
  }
}
