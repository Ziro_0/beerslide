import JsonKey from '../classes/data/JsonKey';
import { Game } from '../game';

export default class PreloadState {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly BASE_PATH = 'assets/game-beerslide/';

  private game: Phaser.Game;
  
  private preloadBar: Phaser.Image;

  // ==============================================================================================
  // public
  // ==============================================================================================
  
  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this.game = game;
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    this.game.state.start('MainMenuState');
  }

  // ----------------------------------------------------------------------------------------------
  preload(): void {
    this.preloadBar = this.game.add.image(
      this.game.world.centerX,
      this.game.world.centerY,
      'preload_bar');
    this.preloadBar.anchor.set(0.5);

    this.game.load.setPreloadSprite(this.preloadBar);

    // load assets
    this.loadAtlases();
    this.loadJsonFiles();
    this.loadSounds();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private loadJson(key: string): void {
    const path = `${PreloadState.BASE_PATH}json/`;
    this.game.load.json(key, `${path}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadJsonFiles(): void {
    [
      JsonKey.OBSTACLES_JSON_KEY,
      JsonKey.OBSTACLE_BOARDS_JSON_KEY,
    ].forEach((key) => {
      this.loadJson(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadAtlas(key: string): void {
    const baseUrl = `${PreloadState.BASE_PATH}atlases/${key}`;
    this.game.load.atlas(key, `${baseUrl}.png`, `${baseUrl}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadAtlases(): void {
    [
      JsonKey.GRAPHICS_BACKGROUNDS_0,
      JsonKey.GRAPHICS_BACKGROUNDS_1,
      JsonKey.GRAPHICS_IMAGES,
    ].forEach((key) => {
      this.loadAtlas(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSound(key: string): void {
    const baseUrl = `${PreloadState.BASE_PATH}sounds/${key}`;
    const mp3Url = `${baseUrl}.mp3`;
    const oggUrl = `${baseUrl}.ogg`;
    const urls = [
      mp3Url,
      oggUrl
    ];

    this.game.load.audio(key, urls);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSounds(): void {
    const game: Game = <Game>this.game;
    game.beerslideConfig.sounds.forEach((key) => {
      this.loadSound(key);
    });
  }
}