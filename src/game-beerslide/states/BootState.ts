export default class BootState {
    game: Phaser.Game;

    constructor(game: Phaser.Game) {
        this.game = game;
    }

    init() {
        // Responsive scaling
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        // Center the game
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;

        // Portrait orientation
        this.game.scale.forceOrientation(false, true);
    }

    preload() {
        this.game.load.image('preload_bar', 'assets/game-piano/images/preload_bar.png');
    }

    create() {
        this.game.state.start('PreloadState');
    }
}
