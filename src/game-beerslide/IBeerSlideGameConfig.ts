import { IPoint, IRect } from './util/util';

// ================================================================================================
export interface IPlayerSounds {
  /**
   * Specifies the keys of sounds used when the player crashes on the ground.
   */
  crashes?: string | string[];

  /**
   * Specifies the keys of sounds used when the player jumps.
   */
  jumps?: string | string[];
  
  /**
   * Specifies the keys of sounds used when the player takes a step.
   */
  steps?: string | string[];
}

// ================================================================================================
export interface IImageKeyFramePair {
  /**
   * Key of the atlas that contains the frame image.
   */
  key?: string;

  /**
   * Key of the frame image.
   */
  frame?: string;
}

// ================================================================================================
export default interface IBeerSlideGameConfig {
  /**
   * Image frame key that makes up the background image. This image does not scroll, and
   * make sure it is big enough to fill the entire screen.
   */
  backgroundImageKey?: string;

  /**
   * Array of the image frame keys that make up the scrolling background images.
   */
  backgroundImageFrameKeys: IImageKeyFramePair[];

  /**
   * Max speed, in pixels per frame, that the background image scrolls.
   * The value is governed by current game speed.
   */
  backgroundMaxScrollSpeed: number;

 /**
  * Min speed, in pixels per frame, that the background image scrolls.
  * The value is governed by current game speed.
  */
  backgroundMinScrollSpeed: number;

  /**
   * If `true`, the bounding boxes for the player, platforms, and avoidable objects are drawn.
   */
  debugShowBoundingBoxes?: boolean;

  /**
   * Specifies the bounds (`x`=width, `y`=height) in which impact animations are created. The
   * position of the impact animation presentation is the center of the bounds.
   */
  impactAnimationBounds?: IPoint;

  /**
   * The number of impact animations that are created while the impact animation presentation
   * is active.
   */
  impactAnimationNumHits?: number,
  
  /**
   * The rate, in ms, at which impact animations are created while an impact animation presentation
   * is active.
   */
  impactAnimationRateMs?: number,

  /**
   * A 2D array of image frame keys used for impact animations. An "impact animation" is shown when
   * a player collides with an obstacle. Each sub-array is a sequence of image frame keys to
   * be used in individual impact animations.
   */
  impactFrameKeys: string[][];

  /**
   * An array that includes two numbers: a min scale for the impact animations, and a max scale.
   * When each impact animation is crerated, a scale is chosed at random within this range.
   */
  impactScale?: number[];

  /**
   * Array of the atlas keys and image frame keys that make up the platforms
   */
  platformImageFrameKeys: IImageKeyFramePair[];

  /**
   * Max speed, in pixels per frame, that the ground platform scrolls.
   * The value is governed by current game speed.
   */
  groundMaxScrollSpeed: number;

  /**
   * Min speed, in pixels per frame, that the ground platform scrolls.
   * The value is governed by current game speed.
   */
  groundMinScrollSpeed: number;

  /**
   * Animation speed, in frames per second, that the player runs when the running at the slowest
   * speed. The value is governed by current game speed.
   */
  playerAnimRateSlowFps: number;
  
  /**
   * Animation speed, in frams per second, that the player runs when the running at the fastest
   * speed. The value is governed by current game speed.
   */
  playerAnimRateFastFps: number;

  /**
   * Defines the bounding box of the player. By default, the box will be the same size as the
   * first frame of the player's sprite. Also, not that setting the player's position is relative
   * to the bounding box, NOT the player's sprite.
   */
  playerBoundingBox?: IRect;

  /**
   * The name of the default frame image the player uses. Running and jumping frames will use this
   * if they don't have their own frames defined.
   */
  playerDefaultFrame: string;

  /**
   * Defines the bounding box of the player when defeated. By default, the box will be the same
   * size as `playerBoundingBox`. See `playerBoundingBox` for more details about setting
   * bounding boxes.
   */
  playerDefeatedBoundingBox?: IRect;

  /**
   * The velocity the player's "knockback" when defeated. x represents the horizontalknockback
   * velocity, while y is similar to "jump power"
   */
  playerDefeatedKnockback?: IPoint;

  /**
   * The running and defeated frames of the player might not be the same size, or the avatar within
   * the bounding box of the image may be off. If so, you can use this to offset the defeated
   * position of the player, so that the transition from running to defeated look more fluid.
   */
  playerDefeatedPositionOffset?: IPoint;

  /**
   * Animation frames the player uses when falling back after running into a wall.
   */
  playerFallBackFrames: string[];

  /**
   * Gravity of the player.
   */
  playerGravity?: number;

  /**
   * Animation frames the player uses while on the ascent of a jump.
   */
  playerJumpAscendFrames?: string[];

  /**
   * Animation frames the player uses while on the descent of a jump.
   */
  playerJumpDescendFrames?: string[];

  /**
   * The jumping power of the player. You'll need to experiment with different values to find what
   * feels right to you.
   */
  playerJumpPower: number;

  /**
   * Animation frames the player uses while running.
   */
  playerRunFrames?: string[];

  /**
   * The start position of the player's avatar.
   */
  playerStartPosition: IPoint;

  /**
   * The index-based frames of the player's running animations on which a "step" sound should play.
   */
  playerStepFrames?: number[];

  /**
   * Specifies the keys of sounds used by the player's avatar. See `IPlayerSounds` for more info.
   */
  playerSounds?: IPlayerSounds;

  /**
   * Animation frames the player uses when tripping. A player "trips" if colliding witn an obstacle
   * while airborne.
   */
  playerTrippedFrames: string[];

  /**
   * Defines the bounding box of the player when hitting the ground after being tripped. By
   * default, the box will be the same size as `playerBoundingBox`. See `playerBoundingBox`
   * for more details about setting bounding boxes.
   */
  playerTrippedLandBoundingBox?: IRect;

  /**
   * Animation frames the player uses when hitting the ground after being tripped.
   */
  playerTrippedLandFrames: string[];
 
  /**
   * The running and tripped frames of the player might not be the same size, or the avatar within
   * the bounding box of the image may be off. If so, you can use this to offset the tripped
   * position of the player, so that the transition from running to tripped look more fluid.
   */
  playerTrippedPositionOffset?: IPoint;

  /**
   * Defines the bounding box of the player while triped. By default, the box will be the same
   * size as `playerBoundingBox`. See `playerBoundingBox` for more details about setting
   * bounding boxes.
   */
  playerTrippedBoundingBox?: IRect;

  /**
   * Number of points gain periodically. Default is `1`.
   */
  pointsGainedPerStep?: number;

  /**
   * Number of points lost when you lose a life. Default is `3`.
   */
  pointsLostWhenLifeLost?: number;

   /**
   * If `false`, the obstacle that collided with the player is not removed. Default is `true`.
   */
  shouldRemoveObstacleOnCollision?: boolean;

  /**
   * Array of sounds to load. It's recommended to provide both an MP3, and an OGG file (of the
   * same name) for each sound you want to add.
   */
  sounds: string[];

  /**
   * The start game speed. Should be between 0 (slowest), and 1 (fastest).
   */
  startSpeed: number;

  /**
   * Time, in seconds, that the player's speed remains constant until it changes. The player's speed
   * will change each time this number of seconds passes. If the value is 0, the speed will never
   * change (from the default `startSpeed`). The default value is 8.0 seconds.
   */
  timeWithinSpeed: number;

  /**
   * If `false`, the Phaser came will not flash and shake when a collision happens. Default is `true`.
   */
  useCameraEffectsOnCollision?: boolean;

  /**
   * If `true`, messages will appear near top-center when the speed is changing.
   * They are single-frame images, and hsold be named:
   * `speed-msg-accel`
   * `speed-msg-decel`
   */
  useSpeedChangeMessages?: boolean;
}
